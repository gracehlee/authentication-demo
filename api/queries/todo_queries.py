"""
Database Queries for Todos
"""
import os
import psycopg
from psycopg_pool import ConnectionPool
from psycopg.rows import class_row
from typing import Optional
from models.todos import TodoRequest, TodoResponse
from utils.exceptions import TodoDatabaseException

DATABASE_URL = os.environ.get("DATABASE_URL")
if not DATABASE_URL:
    raise ValueError("DATABASE_URL environment variable is not set")
pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class TodoQueries:
    """
    Class containing queries for the Todo table

    Can be dependency injected into a route like so

    def my_route(todoQueries: TodoQueries = Depends()):
        # Here you can call any of the functions to query the DB
    """

    def get_all(self) -> list[TodoResponse]:
        """
        Returns all the todos in the database

        Raises a TodoDatabaseException if there is a psycopg error
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(TodoResponse)) as cur:
                    cur.execute(
                        """
                        SELECT
                            *
                        FROM todos
                        """
                    )
                    todos = cur.fetchall()
        except psycopg.Error:
            raise TodoDatabaseException("Error getting Todos")
        return todos

    def get_all_by_user_id(self, user_id: int) -> list[TodoResponse]:
        """
        Returns all the Todos for a specific user id

        Raises a TodoDatabaseException if there is a psycopg error
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(TodoResponse)) as cur:
                    cur.execute(
                        """
                        SELECT
                            *
                        FROM todos
                        WHERE user_id = %s
                        """,
                        [user_id],
                    )
                    todos = cur.fetchall()
        except psycopg.Error:
            raise TodoDatabaseException("Error geting Todos")
        return todos

    def get_by_id(self, id: int) -> Optional[TodoResponse]:
        """
        Gets a Todo by it's id

        Returns None if it can't find one

        Raises a TodoDatabaseException if there is a psycopg error
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(TodoResponse)) as cur:
                    cur.execute(
                        """
                        SELECT
                            id,
                            action,
                            description,
                            value,
                            user_id
                        FROM todos
                        WHERE id = %s
                        """,
                        [id],
                    )
                    todo = cur.fetchone()
                    if not todo:
                        return None
        except psycopg.Error as e:
            print(e)
            raise TodoDatabaseException(f"Error getting Todo {id}")
        return todo

    def create_todo(self, todo: TodoRequest, user_id: int) -> TodoResponse:
        """
        Creates a new Todo for a specific User

        Raises a TodoDatabaseException if there is a psycopg error
        """
        try:
            with pool.connection() as conn:
                with conn.cursor(row_factory=class_row(TodoResponse)) as cur:
                    cur.execute(
                        """
                        INSERT INTO todos (
                            action,
                            description,
                            value,
                            user_id
                        ) VALUES (
                            %s, %s, %s, %s
                        )
                        RETURNING id, action, description, value, user_id;
                        """,
                        [
                            todo.action,
                            todo.description,
                            todo.value,
                            user_id,
                        ],
                    )
                    new_todo = cur.fetchone()
                    if not new_todo:
                        raise TodoDatabaseException("Could not insert Todo")
        except psycopg.Error as e:
            print(e)
            raise TodoDatabaseException("Could not insert Todo")
        return new_todo
