from . import down, up, UpState, DownState
import os
import sys
from psycopg_pool import AsyncConnectionPool
from psycopg.rows import TupleRow
from psycopg import AsyncConnection



async def migrate() -> None:
    db_url = os.environ.get("DATABASE_URL")
    if not db_url:
        raise ValueError("DATABASE_URL environment variable is not set")

    async with AsyncConnectionPool(
        connection_class=AsyncConnection[TupleRow],
        conninfo=db_url,
        reconnect_timeout=120,
    ) as pool:
        if len(sys.argv) < 2:
            print("Command: up|down [amount]")
            exit(1)
        direction = sys.argv[1]
        amount = sys.argv[2] if len(sys.argv) > 2 else None
        if direction == "up":
            up_amount: UpState = "LATEST"
            if amount is None:
                up_amount = "LATEST"
            else:
                try:
                    up_amount = int(amount)
                except ValueError:
                    print(f"Unknown amount {amount}")
            await up(pool, to=up_amount)
        elif direction == "down":
            down_amount: DownState = "ZERO"
            if amount is None:
                down_amount = 1
            elif amount == "zero":
                down_amount = "ZERO"
            else:
                try:
                    down_amount = int(amount)

                except ValueError:
                    print(f"Unknown amount {amount}")
            await down(pool, to=down_amount)


if __name__ == "__main__":
    from asyncio import run

    run(migrate())
