"""
User API Router
"""
from fastapi import APIRouter, Depends, HTTPException, status
from queries.todo_queries import (
    TodoQueries,
    TodoRequest,
    TodoResponse,
)
from utils.exceptions import TodoDatabaseException
from models.users import UserResponse
from utils.authentication import try_get_jwt_user_data, JWTUserData
from typing import Optional

# Note you can put a tag in here and it will help organize your routes
# Also a prefix means you can leave that prefix out when you define
# the rest of the routes
router = APIRouter(tags=["Todos"], prefix="/api/todos")


# Note this is "" because of the prefix
@router.post("", response_model=TodoResponse)
def create_todo(
    todo: TodoRequest,
    todo_queries: TodoQueries = Depends(),
    user: Optional[JWTUserData] = Depends(try_get_jwt_user_data),
) -> TodoResponse:
    """
    Creates a new todo

    If you are not logged in, returns a 401
    """
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not Allowed to create new Todos",
        )
    try:
        new_todo = todo_queries.create_todo(todo, user.id)
    except TodoDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail=str(e)
        )
    return new_todo


# Note this is "" because of the prefix
@router.get("", response_model=list[TodoResponse])
def get_all(
    queries: TodoQueries = Depends(),
    user: UserResponse = Depends(try_get_jwt_user_data),
) -> list[TodoResponse]:
    """
    Returns the user's todos if logged in

    Returns all the todos if the user isn't logged in
    """
    try:
        if user:
            todos = queries.get_all_by_user_id(user.id)
        todos = queries.get_all()
    except TodoDatabaseException as e:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail=str(e)
        )
    return todos


@router.get("/{id}", response_model=TodoResponse)
def get_by_id(id: int, queries: TodoQueries = Depends()) -> TodoResponse:
    """
    Returns a todo by it's id
    """
    todo = queries.get_by_id(id)
    if not todo:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=f"Todo {id} could not be found",
        )
    return todo
