// @ts-check
import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { useTodos } from '../hooks/useTodos'

function TodoForm() {
    const [action, setAction] = useState('')
    const [description, setDescription] = useState('')
    const [value, setValue] = useState('')
    const { createTodo, isLoading, error } = useTodos()
    const navigate = useNavigate()

    /**
     * @param {React.FormEvent<HTMLFormElement>} e
     */
    async function handleFormSubmit(e) {
        e.preventDefault()

        const newTodo = await createTodo({
            action,
            description,
            value: parseInt(value),
        })

        if (newTodo) {
            navigate('/todos')
        }
    }

    if (isLoading) {
        return <p>Loading...</p>
    }

    return (
        <form onSubmit={handleFormSubmit}>
            {error && <div className="error">{error.message}</div>}
            <ul>
                <li>
                    action
                    <input
                        type="text"
                        name="action"
                        value={action}
                        onChange={(e) => setAction(e.currentTarget.value)}
                    />
                </li>
                <li>
                    description
                    <input
                        type="text"
                        name="description"
                        value={description}
                        onChange={(e) => setDescription(e.currentTarget.value)}
                    />
                </li>
                <li>
                    value
                    <input
                        type="text"
                        name="value"
                        value={value}
                        onChange={(e) => setValue(e.currentTarget.value)}
                    />
                </li>
            </ul>
            <button type="submit">create todo</button>
        </form>
    )
}

export default TodoForm
