// @ts-check
import { useTodos } from '../hooks/useTodos'

function TodoList() {
    const { todos, isLoading, error } = useTodos()

    if (isLoading) {
        return <div>Loading...</div>
    }

    if (error) {
        console.error(error)
        return <div>Couldn't load todos</div>
    }

    return (
        <div>
            <ul>{todos && todos.map((t) => <li key={t.id}>{t.action}</li>)}</ul>
        </div>
    )
}

export default TodoList
