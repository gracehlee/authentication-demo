// @ts-check
import { useState, useEffect } from 'react'
import { createTodo, getTodos } from '../services/todosService'

/**
 * @typedef {import('../types').Todo} Todo
 * @typedef {import('../types').TodoFormData} TodoFormData
 */
export function useTodos() {
    /**
     * @type [Todo[] | undefined, React.Dispatch<React.SetStateAction<Todo[] | undefined>>]
     */
    const [todos, setTodos] = useState()
    /**
     * @type [Error | undefined, React.Dispatch<React.SetStateAction<Error | undefined>>]
     */
    const [error, setError] = useState()
    const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        setIsLoading(true)
        getTodos()
            .then((todos) => {
                setTodos(todos)
                setIsLoading(false)
            })
            .catch((e) => {
                setError(e)
                setIsLoading(false)
            })
    }, [])

    return {
        isLoading,
        error,
        todos,
        /** This function wrapper calls our real createTodo function
         *  And then sets state accordingly
         * @param {TodoFormData} todo
         */
        createTodo: async (todo) => {
            // Start loading
            setIsLoading(true)
            /**
             * @type Todo
             */
            let newTodo
            try {
                // Try to create the Todo
                newTodo = await createTodo(todo)
            } catch (e) {
                // If there's an error, set it
                // in the state and return
                if (e instanceof Error) {
                    setError(e)
                }
                setIsLoading(false)
                return
            }
            // If we made a new todo, update the todos state
            if (newTodo) {
                if (todos) {
                    setTodos([...todos, newTodo])
                } else {
                    setTodos([newTodo])
                }
            }
            // Clear out any previous errors
            setError(undefined)
            // Stop loading
            setIsLoading(false)
            // And return the new todo item
            return newTodo
        },
    }
}
